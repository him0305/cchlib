package dev.android.chanchunhim.input.views.datepicker

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.widget.NumberPicker
import dev.android.chanchunhim.input.R

import java.text.DecimalFormat
import java.util.*

/**
 * Created by him.chan on 27/10/2017.
 */
internal class YearMonthPickerDialog(context: Context, private val onSetListener: OnSetListener?) : AlertDialog(context), DialogInterface.OnClickListener {
    private val np_year: NumberPicker
    private val np_month: NumberPicker

    private var date: Calendar? = null
    private var minDate: Calendar? = null
    private var maxDate: Calendar? = null
    private var hasMonth = true
    private var shortForm = false

    interface OnSetListener {
        fun onYearSet(year: Int)

        fun onMonthSet(month: Int)
    }

    init {
        minDate = Calendar.getInstance()
        minDate!!.set(Calendar.MONTH, 0)
        minDate!!.set(Calendar.YEAR, 2000)

        maxDate = Calendar.getInstance()
        maxDate!!.set(Calendar.MONTH, 11)
        maxDate!!.set(Calendar.YEAR, 2100)

        setTitle(TITLE_MONTH_YEAR)

        val view = LayoutInflater.from(context).inflate(R.layout.component_year_month_picker, null)
        setView(view)

        np_year = view.findViewById(R.id.np_year)
        np_month = view.findViewById(R.id.np_month)

        np_month.minValue = 1
        np_month.maxValue = 12
        np_month.value = 1
        np_month.wrapSelectorWheel = false

        np_year.minValue = minDate!!.get(Calendar.YEAR)
        np_year.maxValue = maxDate!!.get(Calendar.YEAR)
        np_year.value = minDate!!.get(Calendar.YEAR)
        np_year.wrapSelectorWheel = false
        np_year.setOnValueChangedListener { picker, oldVal, newVal -> update() }

        update()

        setButton(DialogInterface.BUTTON_POSITIVE, context.getString(android.R.string.ok), this)
        setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(android.R.string.cancel), this)

        setShortForm(false)
    }

    private fun update() {
        val newVal = np_year.value
        if (newVal > minDate!!.get(Calendar.YEAR) && newVal < maxDate!!.get(Calendar.YEAR)) {
            np_month.minValue = 1
            np_month.maxValue = 12
        } else {
            if (newVal <= minDate!!.get(Calendar.YEAR)) {
                np_month.minValue = minDate!!.get(Calendar.MONTH) + 1
                np_month.maxValue = 12
                np_month.value = minDate!!.get(Calendar.MONTH) + 1
            }
            if (newVal >= maxDate!!.get(Calendar.YEAR)) {
                np_month.minValue = 1
                np_month.maxValue = maxDate!!.get(Calendar.MONTH) + 1
                np_month.value = 1
            }
        }
        np_month.wrapSelectorWheel = false
    }

    fun setHasMonth(hasMonth: Boolean) {
        this.hasMonth = hasMonth

        setTitle(if (hasMonth) if (shortForm) TITLE_MONTH_YEAR_SHORT else TITLE_MONTH_YEAR else TITLE_YEAR)
        np_month.visibility = if (hasMonth) View.VISIBLE else View.GONE
    }

    fun setDate(date: Date) {
        val calendar = Calendar.getInstance()
        calendar.time = date
        this.date = calendar

        np_year.value = calendar.get(Calendar.YEAR)
        np_month.value = calendar.get(Calendar.MONTH) + 1
        np_year.wrapSelectorWheel = false
        np_month.wrapSelectorWheel = false
        update()
    }

    fun setMinDate(date: Date) {
        val calendar = Calendar.getInstance()
        calendar.time = date
        minDate = calendar

        np_year.minValue = calendar.get(Calendar.YEAR)
        np_year.wrapSelectorWheel = false
        update()
    }

    fun setMaxDate(date: Date) {
        val calendar = Calendar.getInstance()
        calendar.time = date
        maxDate = calendar

        np_year.maxValue = calendar.get(Calendar.YEAR)
        np_year.wrapSelectorWheel = false
        update()
    }

    fun setShortForm(isShortForm: Boolean?) {
        if (isShortForm != null) {
            this.shortForm = isShortForm

            np_year.setFormatter(if (isShortForm)
                NumberPicker.Formatter { value ->
                    val s = (value).toString()
                    if (s.length == 4) {
                        s.substring(2, 2)
                    } else {
                        s
                    }
                }
            else
                null)
            np_month.setFormatter(if (isShortForm)
                NumberPicker.Formatter { value -> DecimalFormat("00.#").format(value.toLong()) }
            else
                null)

            setTitle(if (hasMonth) if (shortForm) TITLE_MONTH_YEAR_SHORT else TITLE_MONTH_YEAR else TITLE_YEAR)
        }
    }

    override fun onClick(dialog: DialogInterface, which: Int) {
        when (which) {
            DialogInterface.BUTTON_POSITIVE -> if (onSetListener != null) {
                onSetListener.onYearSet(np_year.value)
                if (hasMonth) {
                    onSetListener.onMonthSet(np_month.value)
                }
            }
            DialogInterface.BUTTON_NEGATIVE -> cancel()
        }
    }

    companion object {
        private val TITLE_MONTH_YEAR_SHORT = "MM YY"
        private val TITLE_MONTH_YEAR = "Month & Year"
        private val TITLE_YEAR = "Year"
    }
}