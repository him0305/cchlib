package dev.android.chanchunhim.input.views.spinner

import android.app.Activity
import android.content.Context
import android.support.v7.widget.ListPopupWindow
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.transitionseverywhere.Rotate
import com.transitionseverywhere.TransitionManager
import dev.android.chanchunhim.input.R
import dev.android.chanchunhim.input.views.base.Input

/**
 * Created by him.chan on 30/10/2017.
 */
class Spinner : Input, View.OnClickListener, AdapterView.OnItemClickListener {
    private var listPopupWindow: ListPopupWindow? = null
    private var iv_arrow: ImageView? = null

    private var keyValueMap: Map<Int, String>? = null
    private var adapter: ArrayAdapter<String>? = null
    var selectedIndex = -1
        private set

    private var onItemSelectedCallback: ((position: Int, key: Int?, value: String?) -> Boolean)? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        et_value!!.setOnClickListener(this)
        et_value!!.keyListener = null

        listPopupWindow = ListPopupWindow(context)
        listPopupWindow!!.width = LayoutParams.WRAP_CONTENT
        listPopupWindow!!.height = LayoutParams.WRAP_CONTENT
        listPopupWindow!!.anchorView = this
        listPopupWindow!!.isModal = true
        listPopupWindow!!.setOnItemClickListener(this)
        listPopupWindow!!.setOnDismissListener {
            TransitionManager.beginDelayedTransition(ll_root, Rotate())
            iv_arrow!!.rotation = 0f
        }

        val dp24 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24f, resources.displayMetrics).toInt()
        iv_arrow = ImageView(context)
        iv_arrow!!.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp)
        iv_arrow!!.scaleType = ImageView.ScaleType.FIT_CENTER
        ll_root!!.addView(iv_arrow, LayoutParams(dp24, dp24))
    }

    private fun show(delay: Boolean) {
        postDelayed({
            listPopupWindow!!.show()

            TransitionManager.beginDelayedTransition(ll_root, Rotate())
            iv_arrow!!.rotation = 180f
        }, if (delay) 200L else 0L)
    }

    override fun onFocusChange(hasFocus: Boolean) {
        if (hasFocus) {
            val currentFocus = (context as Activity).currentFocus
            if (currentFocus != null) {
                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(currentFocus.windowToken, 0)
                show(true)
            } else {
                show(false)
            }
        }

        super.onFocusChange(hasFocus)
    }

    override fun onClick(v: View) {
        show(false)
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        selectedIndex = position

        value = adapter!!.getItem(position)
        onItemSelectedCallback?.let { it(position, getKey(position), getValue(position)) }

        listPopupWindow!!.dismiss()
    }

    fun setKeyValueMap(keyValueMap: Map<Int, String>?) {
        this.keyValueMap = keyValueMap
        if (this.keyValueMap != null) {
            this.keyValueMap = mapOf()
        }
        adapter = ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, keyValueMap!!.values.toList())
        listPopupWindow!!.setAdapter(adapter)
    }

    fun setSelection(index: Int? = null, key: Int? = null, value: String? = null) {
        if (keyValueMap != null && keyValueMap!!.isNotEmpty()) {
            if (index != null && index != -1) {
                selectedIndex = index
                this.value = adapter!!.getItem(index)
            } else if (key != null) {
                setSelection(index = keyValueMap!!.keys.indexOf(key))
            } else if (value != null) {
                setSelection(index = keyValueMap!!.values.indexOf(value))
            }
        }
    }

    fun clearSelection() {
        listPopupWindow!!.clearListSelection()
        value = ""
    }

    fun getKey(index: Int): Int? = if (index != -1) keyValueMap!!.keys.elementAt(index) else null

    fun getValue(index: Int): String? = if (index != -1) keyValueMap!!.getValue(index) else null
}