package dev.android.chanchunhim.input.views.datepicker

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import dev.android.chanchunhim.input.R
import dev.android.chanchunhim.input.views.base.Input
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates

/**
 * Created by him.chan on 27/10/2017.
 */
class DatePicker : Input, DatePickerDialog.OnDateSetListener, YearMonthPickerDialog.OnSetListener, View.OnClickListener {
    private var dateFormat: SimpleDateFormat? = null
    private var type by Delegates.observable<Type?>(null) { _, _, newValue ->
        val pattern = when (newValue) {
            Type.DayMonthYear -> "dd'\u00A0'/'\u00A0'MM'\u00A0'/'\u00A0'yyyy"
            Type.MonthYear -> "MM'\u00A0'/'\u00A0'yyyy"
            Type.YearMonthDay -> "yyyy'\u00A0'/'\u00A0'MM'\u00A0'/'\u00A0'dd"
            else -> null
        }
        if (pattern != null) {
            dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
        }
    }

    // Value
    private var calendar: Calendar? = null
    private var minDate: Calendar? = null
    private var maxDate: Calendar? = null

    // View
    private var datePickerDialog: DatePickerDialog? = null
    private var yearMonthPickerDialog: YearMonthPickerDialog? = null

    private var onDateSetCallback: ((calendar: Calendar) -> Unit)? = null

    val formattedDate: String
        get() {
            return dateFormat!!.format(calendar!!.time)
        }

    val date: Date?
        get() = if (value.isNotEmpty()) calendar!!.time else null

    enum class Type {
        DayMonthYear, MonthYear, YearMonthDay
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.DatePicker, 0, 0)
        try {
            val typeId = a.getInt(R.styleable.DatePicker_cch_type, 0)
            type = when (typeId) {
                0 -> Type.DayMonthYear
                1 -> Type.MonthYear
                2 -> Type.DayMonthYear
                else -> null
            }
        } finally {
            a.recycle()
        }
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        et_value!!.setOnClickListener(this)
        et_value!!.keyListener = null

        calendar = Calendar.getInstance()

        // DatePicker
        initDatePicker()

        // YearMonthPicker
        yearMonthPickerDialog = YearMonthPickerDialog(context, this)
        yearMonthPickerDialog!!.setHasMonth(true)
        yearMonthPickerDialog!!.setDate(calendar!!.time)
        yearMonthPickerDialog!!.setShortForm(false)
    }

    private fun initDatePicker() {
        datePickerDialog = DatePickerDialog(context, this, calendar!!.get(Calendar.YEAR), calendar!!.get(Calendar.MONTH), calendar!!.get(Calendar.DAY_OF_MONTH))
        if (minDate != null) {
            datePickerDialog!!.datePicker.minDate = minDate!!.timeInMillis
        }
        if (maxDate != null) {
            datePickerDialog!!.datePicker.maxDate = maxDate!!.timeInMillis
        }
        datePickerDialog!!.setTitle("")
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        calendar!!.set(Calendar.YEAR, year)
        calendar!!.set(Calendar.MONTH, month)
        calendar!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        value = formattedDate
        onDateSetCallback?.let { it(calendar!!.clone() as Calendar) }

        datePickerDialog!!.setTitle("")
    }

    override fun onYearSet(year: Int) {
        calendar!!.set(Calendar.YEAR, year)
        value = formattedDate
        onDateSetCallback?.let { it(calendar!!.clone() as Calendar) }
    }

    override fun onMonthSet(month: Int) {
        calendar!!.set(Calendar.MONTH, month - 1)
        value = formattedDate
        onDateSetCallback?.let { it(calendar!!.clone() as Calendar) }
    }

    override fun onFocusChange(hasFocus: Boolean) {
        if (hasFocus) {
            val currentFocus = (context as Activity).currentFocus
            if (currentFocus != null) {
                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(currentFocus.windowToken, 0)
            }

            showPicker()
        }

        super.onFocusChange(hasFocus)
    }

    override fun onClick(v: View) {
        showPicker()
    }

    private fun showPicker() {
        when (type) {
            Type.DayMonthYear, Type.YearMonthDay -> datePickerDialog!!.show()
            Type.MonthYear -> yearMonthPickerDialog!!.show()
        }
    }

    fun setDatePickerType(type: Type) {
        this.type = type
    }

    fun getCalendar(): Calendar? {
        return if (value.isNotEmpty()) calendar else null
    }

    fun clearDate() {
        value = ""
    }

    fun setDate(year: Int, month: Int, dayOfMonth: Int) {
        setInitDate(year, month, dayOfMonth)
        value = formattedDate
    }

    fun setDate(calendar: Calendar?) {
        if (calendar != null) {
            setInitDate(calendar.clone() as Calendar)
            value = formattedDate
        }
    }

    fun setDate(date: Date?) {
        if (date != null) {
            val calendar = Calendar.getInstance()
            calendar.time = date
            setInitDate(calendar)
            value = formattedDate
        }
    }

    fun setInitDate(year: Int, month: Int, dayOfMonth: Int) {
        calendar!!.set(Calendar.YEAR, year)
        calendar!!.set(Calendar.MONTH, month)
        calendar!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        initDatePicker()
        yearMonthPickerDialog!!.setDate(calendar!!.time)
    }

    fun setInitDate(calendar: Calendar) {
        this.calendar = calendar.clone() as Calendar

        initDatePicker()
        yearMonthPickerDialog!!.setDate(this.calendar!!.time)
    }

    fun setInitDate(date: Date) {
        calendar!!.time = date

        initDatePicker()
        yearMonthPickerDialog!!.setDate(calendar!!.time)
    }

    fun setMinDate(calendar: Calendar) {
        minDate = calendar.clone() as Calendar

        initDatePicker()
        yearMonthPickerDialog!!.setMinDate(minDate!!.time)
    }

    fun setMaxDate(calendar: Calendar) {
        maxDate = calendar.clone() as Calendar

        initDatePicker()
        yearMonthPickerDialog!!.setMaxDate(maxDate!!.time)
    }

    fun setMinDate(date: Date) {
        minDate = Calendar.getInstance()
        minDate!!.time = date

        initDatePicker()
        yearMonthPickerDialog!!.setMinDate(minDate!!.time)
    }

    fun setMaxDate(date: Date) {
        maxDate = Calendar.getInstance()
        maxDate!!.time = date

        initDatePicker()
        yearMonthPickerDialog!!.setMaxDate(maxDate!!.time)
    }

    fun setDobDateRange() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        calendar.set(Calendar.YEAR, year - 18)
        setMaxDate(calendar)
        setInitDate(calendar)
        calendar.set(Calendar.YEAR, year - 70)
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        setMinDate(calendar)
    }

    fun setTitle(title: String) {
        yearMonthPickerDialog!!.setTitle(title)
    }
}