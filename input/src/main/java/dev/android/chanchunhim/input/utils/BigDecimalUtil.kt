package dev.android.chanchunhim.input.utils

import java.math.BigDecimal
import java.text.DecimalFormat

/**
 * Created by him.chan on 24/10/2017.
 */
class BigDecimalUtil {
    companion object {
        private val NEGATIVE_VALUE_POSTFIX = " CR"
        private val DECIMAL_FORMAT_2d = DecimalFormat("#,###.##;#'$NEGATIVE_VALUE_POSTFIX'")
        private val DECIMAL_FORMAT_0 = DecimalFormat("#,###;#'$NEGATIVE_VALUE_POSTFIX'")

        fun toCurrency(value: BigDecimal?): String? {
            return if (value != null) DECIMAL_FORMAT_2d.format(value) else null
        }
    }
}