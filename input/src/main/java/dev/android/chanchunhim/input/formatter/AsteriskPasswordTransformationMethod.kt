package dev.android.chanchunhim.input.formatter

import android.text.method.PasswordTransformationMethod
import android.view.View


/**
 * Created by him.chan on 24/10/2017.
 */
class AsteriskPasswordTransformationMethod : PasswordTransformationMethod() {
    override fun getTransformation(source: CharSequence?, view: View?): CharSequence {
        return if (source != null) PasswordCharSequence(source) else ""
    }

    private inner class PasswordCharSequence(private val mSource: CharSequence) : CharSequence {

        override fun get(index: Int): Char {
            return '＊'
        }

        override val length: Int
            get() = mSource.length

        override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
            return mSource.subSequence(startIndex, endIndex)
        }
    }
}
