package dev.android.chanchunhim.input.utils

/**
 * Created by him.chan on 24/10/2017.
 */
class StringUtil {
    companion object {
        fun cleanToNumeric(text: String?): String? {
            return text?.replace(Regex("\\D+"), "")
        }

        fun removeSpecialChars(text: String?): String? {
            return text?.replace(Regex("[()+\\-:,. /]"), "")
        }

        fun isNumeric(text: String?): Boolean {
            return text?.matches(Regex("[0-9]+")) ?: false
        }


    }
}