package dev.android.chanchunhim.input.formatter

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import dev.android.chanchunhim.input.utils.BigDecimalUtil
import dev.android.chanchunhim.input.utils.StringUtil
import java.math.BigDecimal
import java.util.*


/**
 * Created by him.chan on 24/10/2017.
 */
class CurrencyFormatter(private val editText: EditText) : TextWatcher {
    private var defaultZero = false
    private var showSymbol = true
    private var allowNonNumeric = false

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        val cleanString = StringUtil.cleanToNumeric(s.toString())

        editText.removeTextChangedListener(this)

        if (StringUtil.isNumeric(cleanString)) {
            val cleanValue = java.lang.Double.parseDouble(cleanString)
            if (cleanValue > 0.0) {
                s.replace(0, s.length, formatValue(cleanValue))
            } else {
                s.replace(0, s.length, if (defaultZero) formatValue(0.0) else "")
            }
        } else if (!allowNonNumeric) {
            s.replace(0, s.length, if (defaultZero) formatValue(0.0) else "")
        }

        editText.addTextChangedListener(this)
    }

    private fun formatValue(value: Double): String {
        return (if (showSymbol) Currency.getInstance(Locale.getDefault()).symbol else "") + BigDecimalUtil.toCurrency(BigDecimal.valueOf(value))
    }

    fun setDefaultZero(defaultZero: Boolean): CurrencyFormatter {
        this.defaultZero = defaultZero
        return this
    }

    fun setShowSymbol(showSymbol: Boolean): CurrencyFormatter {
        this.showSymbol = showSymbol
        return this
    }

    fun setAllowNonNumeric(allowNonNumeric: Boolean): CurrencyFormatter {
        this.allowNonNumeric = allowNonNumeric
        return this
    }
}