package dev.android.chanchunhim.input.views.base

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.support.annotation.DrawableRes
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.text.*
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.transition.TransitionSet
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import dev.android.chanchunhim.input.R
import dev.android.chanchunhim.input.formatter.AsteriskPasswordTransformationMethod
import dev.android.chanchunhim.input.formatter.CurrencyFormatter
import dev.android.chanchunhim.input.utils.StringUtil


/**
 * Created by him.chan on 23/10/2017.
 */
open class Input : LinearLayout, TextWatcher {
    protected var ll_root: LinearLayout? = null
    private var cl_input: ConstraintLayout? = null
    private var iv_icon: ImageView? = null
    private var tv_label: TextView? = null
    protected var et_value: EditText? = null
    private var tv_second_hint: TextView? = null
    private var tv_error: TextView? = null
    private var iv_validator: ImageView? = null

    // Settings
    private var inputType: EInputType? = null

    private var leftIconResId: Int? = null

    private var label: String? = null
    private var defValue: String? = null
    private var hint: String? = null
    private var secondHint: String? = null

    private var hasFloatingLabel = true
    private var keepFloatingLabel = false
    private var floatOnFocus = false
    private var selectAllOnFocus = false

    // Callbacks
    var onValidationCallback: ((view: Input, input: String) -> Boolean)? = null
        set(value) {
            field = value
            validate()
        }
    var onFocusListener: ((view: Input) -> Unit)? = null

    // Appearance
    private var primaryColor: Int? = null
    private var defaultLabelHeight: Int = 0
    private var enableAnimation = true
    private var animationTime: Int? = null
    private var labelFontSize: Float? = null
    private var valueFontSize: Float? = null
    private var labelFontColor: ColorStateList? = null
    private var valueFontColor: ColorStateList? = null
    private var hintFontColor: ColorStateList? = null

    // State
    private var isLabelFloated: Boolean? = null
    private var isValid: Boolean? = null

    // Others
    private val inputFilters = ArrayList<InputFilter>()

    var value: String
        get() {
            if (inputType != null) {
                when (inputType) {
                    EInputType.HKID -> return StringUtil.removeSpecialChars(et_value?.text.toString()) ?: ""
                    EInputType.CreditCard, EInputType.Phone -> return StringUtil.cleanToNumeric(et_value?.text.toString()) ?: ""
                    else -> {
                    }
                }
            }
            return et_value?.text.toString()
        }
        set(value) {
            et_value?.setText(value)
        }

    enum class EInputType {
        Number, Decimal, Phone, Email, Name, Currency, Password, NonPassword, HKID, AccountNo, CreditCard, CVV
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initAttributeSet(attrs)
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initAttributeSet(attrs)
        init()
    }

    private fun initAttributeSet(attrs: AttributeSet) {
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.Input, 0, 0)
        try {
            leftIconResId = a.getResourceId(R.styleable.Input_cch_leftIcon, 0)
            label = a.getString(R.styleable.Input_cch_label)
            defValue = a.getString(R.styleable.Input_cch_value)
            hint = a.getString(R.styleable.Input_cch_hint)
            secondHint = a.getString(R.styleable.Input_cch_secondHint)
            hasFloatingLabel = a.getBoolean(R.styleable.Input_cch_hasFloatingLabel, true)
            keepFloatingLabel = a.getBoolean(R.styleable.Input_cch_keepFloatingLabel, false)
            floatOnFocus = a.getBoolean(R.styleable.Input_cch_floatOnFocus, false)
            selectAllOnFocus = a.getBoolean(R.styleable.Input_cch_selectAllOnFocus, false)
            labelFontSize = a.getDimension(R.styleable.Input_cch_labelTextSize, resources.getDimension(R.dimen.font_size_label))
            valueFontSize = a.getDimension(R.styleable.Input_cch_valueTextSize, resources.getDimension(R.dimen.font_size_value))
            labelFontColor = a.getColorStateList(R.styleable.Input_cch_labelTextColor)
            valueFontColor = a.getColorStateList(R.styleable.Input_cch_valueTextColor)
            hintFontColor = a.getColorStateList(R.styleable.Input_cch_hintTextColor)
            enableAnimation = a.getBoolean(R.styleable.Input_cch_enableAnimation, true)
        } finally {
            a.recycle()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        if (defaultLabelHeight == 0) {
            val originTextSize = tv_label?.textSize
            tv_label?.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelFontSize!!)

            tv_label?.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
            et_value?.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))

            tv_label?.setTextSize(TypedValue.COMPLEX_UNIT_PX, originTextSize!!)

            defaultLabelHeight = tv_label!!.measuredHeight
            cl_input?.layoutParams?.height = (if (hasFloatingLabel) defaultLabelHeight else 0) + et_value!!.measuredHeight
        }
    }

    private fun init() {
        primaryColor = getPrimaryColor()
        animationTime = 150

        // init view
        View.inflate(context, R.layout.component_input, this)

        ll_root = findViewById(R.id.ll_root)
        cl_input = findViewById(R.id.cl_input)
        iv_icon = findViewById(R.id.iv_icon)
        tv_label = findViewById(R.id.tv_label)
        et_value = findViewById(R.id.et_value)
        tv_second_hint = findViewById(R.id.tv_second_hint)
        tv_error = findViewById(R.id.tv_error)
        iv_validator = findViewById(R.id.iv_validator)

        ll_root?.setOnTouchListener { _, motionEvent ->
            et_value!!.dispatchTouchEvent(motionEvent)
            false
        }

        // Label
        tv_label?.setTextColor(et_value?.hintTextColors)
        tv_label?.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelFontSize!!)
        if (labelFontColor != null) {
            tv_label?.setTextColor(labelFontColor)
        }

        // EditText
        et_value?.id = View.generateViewId()
        et_value?.isSaveEnabled = true
        et_value?.addTextChangedListener(this)
        et_value?.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                clearFocus()
                return@OnEditorActionListener true
            }
            false
        })
        et_value?.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            onFocusChange(hasFocus)
            toggleLabel()
        }
        et_value?.setSelectAllOnFocus(selectAllOnFocus)
        et_value?.setTextSize(TypedValue.COMPLEX_UNIT_PX, valueFontSize!!)
        if (valueFontColor != null) {
            et_value?.setTextColor(valueFontColor)
        }
        if (hintFontColor != null) {
            et_value?.setHintTextColor(hintFontColor)
        }

        cl_input!!.removeView(et_value)
        cl_input!!.addView(et_value)

        update()

        et_value?.setText(defValue)
    }

    private fun update() {
        if (leftIconResId != null && leftIconResId != 0) {
            iv_icon?.visibility = View.VISIBLE
            iv_icon?.setImageResource(leftIconResId!!)
        } else {
            iv_icon?.visibility = View.GONE
        }

        tv_label?.text = label
        tv_second_hint?.text = secondHint
    }

    protected open fun onFocusChange(hasFocus: Boolean) {
        onFocusListener?.let { it(this) }
    }

    private fun getPrimaryColor(): Int {
        val typedValue = TypedValue()
        context.theme.resolveAttribute(R.attr.colorPrimary, typedValue, true)
        return typedValue.data
    }

    fun length(): Int {
        return et_value!!.length()
    }

    override fun hasFocus(): Boolean {
        return et_value!!.hasFocus()
    }

    override fun setEnabled(enabled: Boolean) {
        tv_label!!.isEnabled = enabled
        et_value!!.isEnabled = enabled
        super.setEnabled(enabled)
    }

    fun setImeOptions(imeOptions: Int) {
        et_value?.imeOptions = imeOptions
    }

    fun focus() {
        if (et_value?.isFocusable == true) {
            et_value?.requestFocus()
//            et_value?.postDelayed({ (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(et_value, 0) }, 200)
        }
    }

    override fun clearFocus() {
        et_value?.clearFocus()
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(et_value?.windowToken, 0)
        super.clearFocus()
    }

    fun setMaxLength(maxLength: Int) {
        inputFilters.add(InputFilter.LengthFilter(maxLength))
        et_value?.filters = inputFilters.toTypedArray()
    }

    fun setTextSelection(index: Int) {
        et_value?.setSelection(index)
    }

    fun selectAll() {
        et_value?.selectAll()
    }

    fun setSelectAllOnFocus(selectAllOnFocus: Boolean) {
        et_value?.setSelectAllOnFocus(selectAllOnFocus)
    }

    fun setLeftIcon(@DrawableRes leftIconResId: Int?) {
        this.leftIconResId = leftIconResId
        update()
    }

    fun setLabel(label: String) {
        this.label = label
        update()
    }

    fun setHasFloatingLabel(hasFloatingLabel: Boolean) {
        this.hasFloatingLabel = hasFloatingLabel
        update()
    }

    fun setErrorText(errorText: String?) {
        if (errorText != null && !errorText.isEmpty()) {
            tv_error!!.text = errorText
            tv_error!!.visibility = View.VISIBLE
        } else {
            tv_error!!.text = ""
            tv_error!!.visibility = View.GONE
        }
    }

    fun setOnEditorActionListener(l: TextView.OnEditorActionListener) {
        et_value?.setOnEditorActionListener(l)
    }

    fun addTextChangedListener(watcher: TextWatcher) {
        et_value?.addTextChangedListener(watcher)
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        toggleLabel()
        tv_second_hint!!.visibility = if (secondHint != null && !secondHint!!.isEmpty() && s.isEmpty()) View.VISIBLE else View.GONE
        setErrorText(null)

        validate()
    }

    private fun validate() {
        val isValid = onValidationCallback?.let { it(this, et_value!!.text.toString()) }
        if (this.isValid != isValid) {
            this.isValid = isValid
            if (isValid != null) {
                iv_validator?.setImageResource(if (isValid) R.drawable.ic_check_black_24dp else R.drawable.ic_close_black_24dp)
                iv_validator?.setColorFilter(if (isValid) primaryColor!! else ContextCompat.getColor(context, android.R.color.holo_red_dark))
                iv_validator?.visibility = View.VISIBLE
            } else {
                iv_validator?.visibility = View.GONE
            }
        }
    }

    private fun toggleLabel() {
        val float = !TextUtils.isEmpty(label) && hasFloatingLabel && (keepFloatingLabel || et_value!!.text.isNotEmpty() || (floatOnFocus && et_value!!.hasFocus()))
        if (isLabelFloated == float) {
            return
        }
        isLabelFloated = float

        // EditText Hint
        et_value?.hint = if (!TextUtils.isEmpty(hint) && isLabelFloated == true) hint else ""

        // Constraint
        val set = ConstraintSet()
        set.clone(cl_input)
        if (isLabelFloated == true) {
            set.connect(tv_label!!.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
            set.connect(tv_label!!.id, ConstraintSet.BOTTOM, et_value!!.id, ConstraintSet.TOP)

            set.setVerticalBias(et_value!!.id, 1f)
        } else {
            set.connect(tv_label!!.id, ConstraintSet.TOP, et_value!!.id, ConstraintSet.TOP)
            set.connect(tv_label!!.id, ConstraintSet.BOTTOM, et_value!!.id, ConstraintSet.BOTTOM)
            set.constrainDefaultHeight(tv_label!!.id, ConstraintSet.MATCH_CONSTRAINT_SPREAD)
            set.setVerticalBias(tv_label!!.id, 0.5f)

            set.setVerticalBias(et_value!!.id, 0.5f)
        }

        // Animation
        if (enableAnimation) {
            val transitionSet = TransitionSet()
            transitionSet.addTransition(ChangeBounds())
            transitionSet.duration = animationTime!!.toLong()
            TransitionManager.beginDelayedTransition(cl_input, transitionSet)

            // Label Size Animation
            val startValue = if (isLabelFloated == true) valueFontSize!! else labelFontSize!!
            val endValue = if (isLabelFloated == true) labelFontSize!! else valueFontSize!!
            val animator = ValueAnimator.ofFloat(startValue, endValue)
            animator.duration = animationTime!!.toLong() + 50
            animator.addUpdateListener({ valueAnimator ->
                tv_label!!.setTextSize(TypedValue.COMPLEX_UNIT_PX, valueAnimator.animatedValue as Float)
            })
            animator.start()
        } else {
            tv_label!!.setTextSize(TypedValue.COMPLEX_UNIT_PX, if (isLabelFloated == true) labelFontSize!! else valueFontSize!!)
        }

        set.applyTo(cl_input)
    }

    fun setInputType(input: EInputType?) {
        inputType = input
        if (input != null) {
            when (input) {
                EInputType.Number -> et_value?.setRawInputType(Configuration.KEYBOARD_12KEY) // Allow alphabet with numeric keyboard
                EInputType.Decimal -> et_value?.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                EInputType.Phone -> {
                    et_value?.setRawInputType(Configuration.KEYBOARD_12KEY)
//                    et_value?.addTextChangedListener(SpaceEveryFourFormatter(et_value))
                    setMaxLength(9)
                }
                EInputType.Email -> et_value?.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                EInputType.Name -> {
                    inputFilters.add(InputFilter.LengthFilter(25))
                    inputFilters.add(InputFilter.AllCaps())
                    inputFilters.add(InputFilter { source, _, _, _, _, _ ->
                        if (source == "") { // for backspace
                            return@InputFilter source
                        }
                        if (source.toString().matches("[a-zA-Z ]+".toRegex())) {
                            source
                        } else ""
                    })
                    et_value?.filters = inputFilters.toTypedArray()
                    et_value?.inputType = EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS or EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                }
                EInputType.Currency -> {
                    et_value?.setRawInputType(Configuration.KEYBOARD_12KEY)
                    et_value?.addTextChangedListener(CurrencyFormatter(et_value!!))
                }
                EInputType.Password -> {
                    et_value?.inputType = EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS or EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                    et_value?.transformationMethod = AsteriskPasswordTransformationMethod()
                    setMaxLength(8)
                }
                EInputType.NonPassword -> et_value?.transformationMethod = null
                EInputType.HKID -> {
//                    et_value?.addTextChangedListener(HKIDFormatter(et_value))

                    inputFilters.add(InputFilter.AllCaps())
                    et_value?.filters = inputFilters.toTypedArray()
                    et_value?.inputType = EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS or EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                }
                EInputType.AccountNo -> {
                    et_value?.setRawInputType(Configuration.KEYBOARD_12KEY)
//                    et_value?.addTextChangedListener(AccountNumberFormatter(et_value))
                }
                EInputType.CreditCard -> {
                    et_value?.setRawInputType(Configuration.KEYBOARD_12KEY)
//                    et_value?.addTextChangedListener(SpaceEveryFourFormatter(et_value))
                    setMaxLength(19)
                }
                EInputType.CVV -> {
                    et_value?.setRawInputType(Configuration.KEYBOARD_12KEY)
                    setMaxLength(3)
                }
            }
        }
    }
}