package dev.android.chanchunhim.cchlibs

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dev.android.chanchunhim.input.views.base.Input
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sp.setKeyValueMap(mapOf(Pair(1, "One"), Pair(2, "Two"), Pair(21321, "sauidisofdghsjkiuyfidospiduyf")))

        i_test.onValidationCallback = { view: Input, input: String ->
            input == "123"
        }
    }
}
